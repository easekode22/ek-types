import { Image } from './common';
import { User } from './user';
export interface Instructor {
  id?: string;
  message?: string;
  webUser?: User;
  images?: Image[];
}
