import { Image } from './common';

export interface Config {
  websiteName?: string;
  websiteTagline?: string;
  websiteUrl?: string;
  websiteDescription?: string;
  logo?: Image;
  logoSecondary?: Image;
}
