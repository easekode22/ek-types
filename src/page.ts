import { Image } from './common';
import { Course, MasterClass } from './course';
import { ProjectDetail } from './projectDetail';
import { Testimonial } from './testimonial';
import { ToolsTechnologies } from './toolsTechnologies';
import { WebUser } from './user';

export enum PageType {
  LANDING = 'LANDING',
  COURSE_LISTING = 'COURSE_LISTING',
  COURSE_DETAILS = 'COURSE_DETAILS',
}

export type Banner = {
  title: string;
  description: string;
  image: Image;
  ctaText: string;
  link: string;
};

export type BannerType<T extends string> = {
  [key in T]: Banner;
};

export type Stat = {
  icon: Image;
  stat: string;
  description: string;
};

export type PageStat<T extends string> = {
  [key in T]: Stat[];
};

export type PageContent = {
  pageName?: PageType;
  banners?: Banner[];
  courses?: Course[];
  testimonials?: Testimonial[];
  stats?: Stat[];
  instructors?: WebUser[];
  projectDetails?: ProjectDetail[];
  toolsTechnologies?: ToolsTechnologies[];
  masterClasses?: MasterClass[];
};

export type PageContents = {
  [key in PageType]: PageContent;
};
