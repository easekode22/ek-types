import { Image } from './common';

export interface User {
  name?: string;
  desgination?: string;
  profilePic?: Image;
  linkedIn?: string;
  twitter?: string;
  facebook?: string;
}

export type WebUser = User;
